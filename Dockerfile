FROM carlosfarah/php-nginx:latest
MAINTAINER Carlos Farah "carlosfarah@gmail.com"

RUN apt-get update -y && apt-get install -y vim nginx php5-fpm php5-intl php-apc php5-gd php5-intl php5-mysqlnd php5-pgsql php-pear php5-cli && rm -rf /var/lib/apt/lists/*

RUN sed -i 's/user  nginx/user  www-data/g' /etc/nginx/nginx.conf

RUN echo "catch_workers_output = yes" >> /etc/php5/fpm/php-fpm.conf

RUN mkdir -p /wwwroot/app

ADD default /etc/nginx/sites-available/default

ADD index.php /wwwroot/app

CMD service php5-fpm start && nginx -g "daemon off;"

EXPOSE 80
