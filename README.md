# README #

Essa imagem foi criada com base da imagem oficial do Ubuntu disponibilizada no Hub Docker https://hub.docker.com/_/ubuntu/

O projeto de manutenção da imagem se encontra em: https://bitbucket.org/carlosfarah/docoker

### Para que serve esse repositório? ###

* agilizar a disponibilização de ambientes

### Como trabalhar com a imagem? ###

1 - Efetuar o clone do projeto ou download
2 - Executar o build **$ sudo docker build -t carlosfarah/php-nginx .**
3 - Iniciar o container **docker run --name teste -p 127.0.0.1:8080:80 -d carlosfarah/php-nginx**
4 - Acessar em seu navegador o localhost:8080

### Explicando a execução da montagem do ambiente ###
Iniciamos definindo a origem da imagem: 
* FROM carlosfarah/php-nginx:latest

Definindo o mantedor do projeto:
* MAINTAINER Carlos Farah "carlosfarah@gmail.com"

Na sequência os comando RUN informa que alguns comandos deverão ser executados no terminal durante o processo.

Com o comando ADD transferimos  os arquivos default e index.php para os devidos destinos de configuração.

Por fim é enviado o comando para que seja iniciado o php5-fpm e o nginx

E concluindo o arquivo em função do Beanstalk da Amazon expomos uma porta de acesso.
